# Esl Code Challenge

This is an implementation based on the design https://www.figma.com/file/PIQ8o7nSFDt8o1Jl7kzKXPO5/Eng-Exercise-Design

You can check this application using this url: https://xenodochial-meninsky-b48e2a.netlify.com/177160

## Preview of the app

The front end shows the following information from the ELS API's:
 
 - The name of the tournament
 - The start date of the tournament
 - A list of matches of the tournament, including for each match
 - The time of the match
 - The contestants of the match
 - Matches results
 - Scores of the contestants

The app has two routes:

- /:leagueID
- /

## Screenshots

### Desktop

![desktop](https://bitbucket.org/thaisrodrigues/els-code-test/raw/f79f2662d826c4c58fdba67f1021ad8dcce6e8fb/screenshots/desktop-version.png "DesktopVersion")

### Mobile

![mobile](https://bitbucket.org/thaisrodrigues/els-code-test/raw/2cb4ff2f41f8c0c9de65b8b56bb13674c3af43e0/screenshots/mobile-version.png "Mobile version")

## File Structured
The current app has  the following structured:
### Frontend

+ els-code-test
    + public
    + src
      + actions
      + components  
      + containers
      + layout
      + reducers
      + store
      + theme
		

## Installation

First clone the repository

```
git clone https://bitbucket.org/thaisrodrigues/youvisit-app.git
```

### Frontend
Go inside the project folder and run:

```
yarn  
```

After everything was installed run:

```
yarn start
```

The app should  be started on port 3000



PS: To avoid CORS problems, I am using cors-anywhere to enable cross-origin requests to the ESL API.

## This app was Built using the following libraries:

* React/Redux 
* React JSS
* Axios
* Create React app
* React Router
* History
* Moment
* Prop-types
* react-id-generator
* Redux thunk


## Authors

* ** Made by Thais Rodrigues ** 