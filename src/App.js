import React, { Component } from 'react';
import { ThemeProvider } from 'react-jss';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import theme from './theme/theme';

import LeagueContainer from './containers/LeagueContainer';
import Layout from './Layout/Layout';
import configureStore from './store';

const store = configureStore();
export const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}> 
        <Provider store={store}> 
          <Router history={history}>
            <Layout> 
              <Switch>
                <Route path="/:leagueID" component={LeagueContainer} />
                <Route component={LeagueContainer} /> {/* in case of route not found */}
              </Switch>
            </Layout>
          </Router>
        </Provider>
      </ThemeProvider>
    );
  }
}

export default App;
