import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

//import styles
import { styles } from './LayoutStyles';

//
// This component is responsible for load 
// the general layout of all routers
//
function Layout({ classes, children }) {
  return (
    <div
      className={classes.root}
    >
      {children}
    </div>
  );
}
Layout.propTypes = {
  classes:  PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

const StyledPageLayout = injectSheet(styles)(Layout);
export default StyledPageLayout;