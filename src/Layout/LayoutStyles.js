export const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    margin: '20px 1em 1em 1em',
    "@media (min-width: 1024px)": {
      margin: '1em 30em 3em 30em',
    },
  },
});