import axios from 'axios';

const urlCors = 'https://cors-anywhere.herokuapp.com/'; // Fix Cors Problem

const apiBaseUrls = {
  tournament: `${urlCors}https://api.eslgaming.com/play/v1/leagues/leagueID`,
  contestant: `${urlCors}https://api.eslgaming.com/play/v1/leagues/leagueID/contestants`,
  results: `${urlCors}https://api.eslgaming.com/play/v1/leagues/leagueID/results`,
}
export const getTournamentById = leagueID => dispatch => (
  new Promise((resolve, reject) => {
    return axios.get(getApiUrl('tournament', leagueID))
      .then(({ data }) => {
        resolve(dispatch({
          type: 'LOAD_TOURNAMENT',
          payload: {
            tournamentName: data.name.full,
            tournamentDate: data.timeline.inProgress.begin,
          }
        }));
      })
      .catch(({ message }) => {
        resolve(dispatch({
          type: 'ERROR',
          error: message,
        }));
      });
  })
);
export const getContestantsById = leagueID => dispatch => (
  new Promise((resolve, reject) => {
    return axios.get(getApiUrl('contestant', leagueID))
      .then(({ data }) => {
        resolve(dispatch({
          type: 'LOAD_CONTESTANTS',
          payload: data,
        }));
      })
      .catch(({ message }) => {
        resolve(dispatch({
          type: 'ERROR',
          errorMsg: message,
        }));
      });
  })
);
export const getLeagueResultsById = leagueID => dispatch => (
  new Promise((resolve, reject) => {
    return axios.get(getApiUrl('results', leagueID))
      .then(({ data }) => {
        resolve(dispatch({
          type: 'LOAD_RESULTS',
          payload: data,
        }));
      })
      .catch(({ message }) => {
        resolve(dispatch({
          type: 'ERROR',
          errorMsg: message,
        }));
      });
  })
);
const getApiUrl = (name, leagueID) => {
  return apiBaseUrls[name].replace('leagueID', leagueID);
}