import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

//import styles
import { styles } from './DropDownStyle';

class Dropdown extends Component {
  render() {
    const {
      handleOnclick,
      sortAsc,
      classes,
    } = this.props;
    return (
      <div
        className={classes.root}
      >
        <button
          className={classes.dropdown}
          onClick={handleOnclick}
        >
          Date&nbsp;&nbsp;
          {
            sortAsc ?
              "\u25B2" : 
              "\u25BC"
          }
        </button>
      </div>
    );
  }
}
Dropdown.defaultProptypes = {
  sortAsc: true,
}
Dropdown.propTypes = {
  classes:  PropTypes.object.isRequired,
  sortAsc: PropTypes.bool,
  handleOnclick: PropTypes.func.isRequired,
}
const StyledDropdown = injectSheet(styles)(Dropdown);
export default StyledDropdown;