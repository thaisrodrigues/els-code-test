export const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  dropdown: {
    borderRadius: 0,
    backgroundColor: 'white',
    padding: '.5em',
    border: '1px solid #ccc',
    fontSize: '14px',
    marginBottom: '1em',
    cursor: 'pointer',
  },
});