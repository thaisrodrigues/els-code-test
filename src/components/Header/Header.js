import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import injectSheet from 'react-jss';

//import styles
import { styles } from './HeaderStyle';

function Header({
  nameLeague,
  startDate,
  classes,
}) {
  return (
    <div
      className={classes.root}
    >
      <h1>
        {nameLeague}
      </h1>
      <span
        className={classes.span}
      >
        {moment(startDate).format('Do MMM YYYY')}
      </span>
    </div>
  );
};
Header.propTypes = {
  nameLeague: PropTypes.string.isRequired,
  startDate: PropTypes.string.isRequired,
  classes:  PropTypes.object.isRequired,
}
const StyledHeader = injectSheet(styles)(Header);
export default StyledHeader;