export const styles = theme => ({
  root: {
    background: 'white',
    borderTop: `6px solid ${theme.colors.green}`,
    padding: theme.spacing,
    borderRadius: theme.borderRadius,
    width: '100%',
  },
  span: {
    fontSize: theme.fontSize.span,
  },
});