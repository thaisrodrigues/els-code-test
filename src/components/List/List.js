import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import idGenerator from 'react-id-generator';

import ListItem from '../ListItem/ListItem';
import Dropdown from '../DropDown/DropDown';

//import styles
import { styles } from './ListStyle';


class List extends Component {
  render() {
    const { 
      classes, 
      results,
      sortAsc,
      handleOnclick,
    } = this.props;
    return (
      <div
        className={classes.root}
      >
        <Dropdown
          handleOnclick={handleOnclick}
          sortAsc={sortAsc}
        />
        {results.map((item, i) => (
          <ListItem
            item={item}
            key={idGenerator()}
            borderBottomEnabled={results.length !== i+1}
          />
        ))}
      </div>
    );
  }
}
List.propTypes = {
  classes:  PropTypes.object.isRequired,
  results: PropTypes.arrayOf(PropTypes.any).isRequired,
  sortAsc: PropTypes.bool.isRequired,
  handleOnclick: PropTypes.func.isRequired,
}
const StyledList = injectSheet(styles)(List);
export default StyledList;
