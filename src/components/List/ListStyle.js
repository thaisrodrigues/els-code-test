export const styles = theme => ({
  root: {
    background: 'white',
    borderRadius: theme.borderRadius,
    padding: theme.spacing,
    marginTop: theme.spacing,
    width: '100%',
  },
});