import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import moment from 'moment';

//import styles
import { styles } from './ListItemStyles';

function ListItem({
  item,
  classes,
  borderBottomEnabled, // this is necessary to eliminate the border in the last item of the list
}) {
  const rootClass = borderBottomEnabled ? `${classes.root} ${classes.borderBottom}` : classes.root;
  return (
    <div>
      {item && //avoid null values from the list
        <div
          className={rootClass}
        >
          <span
            className={classes.span}
          >
            {moment(item.timeMatch).format('HH:mm')}
          </span>
          <div 
            className={classes.flex}
          >
            <p 
              className={classes.p}
            >
              {item.participants.winner.name}
            </p>
            <p 
              className={`${classes.rigthText} ${classes.flex} ${classes.bold}`}
            >
              {item.participants.winner.points}
            </p>
          </div>
          <div 
            className={classes.flex}
          >
            <p 
              className={`${classes.p} ${classes.teamText}`}
            >
              {item.participants.loser.name}
            </p>
            <p 
              className={`${classes.rigthText} ${classes.flex}`}
            >
              {item.participants.loser.points}
            </p>
          </div>
        </div>
      } 
    </div>
  );
};
ListItem.defaultProps = {
  borderBottomEnabled: true,
  item: undefined,
}
ListItem.propTypes = {
  item: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.object),
  ]),
  classes:  PropTypes.object.isRequired,
  borderBottomEnabled: PropTypes.bool,
}
const StyledListItem = injectSheet(styles)(ListItem);
export default StyledListItem;