export const styles = theme => ({
  flex: {
    display: 'flex',
  },
  root: {
    marginBottom: theme.spacing,
  },
  borderBottom: {
    borderBottom: `1px solid ${theme.colors.gray}`,
  },
  span: {
    fontSize: theme.fontSize.span,
    color: '#818F8F',
  },
  p: {
    fontSize: theme.fontSize.p,
    borderLeft: `6px solid ${theme.colors.green}`,
    borderRadius: '2px 0px 0px 2px',
    paddingLeft: '10px',
    marginTop: '7px',
    width: '90%',
    marginBottom: '10px',
  },
  teamText: {
    marginBottom: '1.25rem',
    borderLeft: `6px solid ${theme.colors.red}`,
  },
  rigthText: {
    width: '10%',
    justifyContent: 'flex-end',
    marginTop: '7px',
    marginBottom: '10px',
  },
  bold: {
    fontWeight: 'bold',
  }
});