import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Header from '../components/Header/Header';
import ListContainer from './ListContainer';

//action import
import { 
  getTournamentById,
  getContestantsById,
  getLeagueResultsById,

} from '../actions';

const standartLeagueID = '177161';

class LeagueContainer extends PureComponent {
  state = {
    isLoading: true,
  }
  componentDidMount() {
     //use a default id in case of no id specified
    const leagueID = this.props.match.params.leagueID || standartLeagueID;
    const {
      getTournamentById,
      getContestantsById,
      getLeagueResultsById,
    } = this.props;
    const dataReqs = [
      getTournamentById(leagueID),
      getContestantsById(leagueID),
      getLeagueResultsById(leagueID),
    ];

    Promise.all([
      dataReqs,
    ]).then(() => {
      setTimeout(() => { 
        this.setState(() => ({
          isLoading: false,
        })); 
      }, 3000);
    });
  }
  render() {
    const { 
      tournament,
      leagueResults,
      contestants,
    } = this.props;

    const {
      isLoading,
    } = this.state;

    let headerTitle;
    if (isLoading) {
      headerTitle = 'Loading ...';
    } else {
      headerTitle = tournament.error || tournament.tournamentName;  //handle error from the api
    }
    return (
      <React.Fragment>
        <Header
          nameLeague={headerTitle}
          startDate={tournament.error ? '' : tournament.tournamentDate}
        />
        <ListContainer
          leagueResults={tournament.error ? [] : leagueResults} //error handling
          contestants={tournament.error ? [] : contestants}
        />
      </React.Fragment>
    );
  }
} 
LeagueContainer.propTypes = {
  tournament:  PropTypes.objectOf(PropTypes.any).isRequired,
  leagueResults: PropTypes.objectOf(PropTypes.any).isRequired,
  contestants: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.object),
  ]).isRequired,
  getTournamentById: PropTypes.func.isRequired,
  getContestantsById: PropTypes.func.isRequired,
  getLeagueResultsById: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
}
const mapStateToProps = state => {
  const {
    contestants,
    leagueResults,
    tournament
  } = state;
  return {
    contestants,
    leagueResults,
    tournament,
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getTournamentById,
    getContestantsById,
    getLeagueResultsById,
  }, 
   dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(LeagueContainer);