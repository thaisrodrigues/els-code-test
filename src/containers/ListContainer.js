import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import List from '../components/List/List';

class ListContainer extends Component {
  state = {
    sortAsc: true,
  }
  getParticipantName = (id) => {
    const {
      contestants
    } = this.props;

    const parcipant = contestants.data && 
      contestants.data.find((participant) => participant.id === id);

    return parcipant ? parcipant.name : '';
  }

  //sort the date
  sortLeagueResults = (array) => {
    const { sortAsc } = this.state;
    if (array && array.length > 0) {
      return array.sort((data1,data2) => {
       
        const time1 = moment(data1.beginAt).valueOf();
        const time2 = moment(data2.beginAt).valueOf();
        let resul;

        //check if is asc or desc
        if (sortAsc) {
          resul = time1 - time2;
        } else {
          resul = time2 - time1;
        }
        return resul;
      }); 
    } else {
      return array;
    }
   
  }

  getGameResultsArray = () => {
    const {
      leagueResults,
    } = this.props;
    const sortedResults = this.sortLeagueResults(leagueResults.data);
    
    //get the participant name by id
    const arrayResults =  sortedResults && sortedResults.map((result) => {
      const timeMatch = result.beginAt;

      //avoid items without points from the api
      if (result.participants[0].points === null || result.participants[1].points === null) {
        return null;
      }
       //get the winner from the participants list
      const winner = result.participants.find((participant) => (
        participant.place === 1
      ));
       //get the loser from the participants list
      const loser = result.participants.find((participant) => (
        participant.place === 2
      ));
      //build the participants array 
      const participants = {
        winner: {
          name: this.getParticipantName(winner ? winner.id : ''), //get the participant name by id
          points: winner ? winner.points[0] : 0,
        },
        loser: {
          name: this.getParticipantName(loser ? loser.id : ''), //get the participant name by id
          points: loser ? loser.points[0] : 0,
        }
      }
      return {
        timeMatch,
        participants,
      }
    });
    
    return arrayResults;
  }
  dropdownOnClick= (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
      sortAsc: !prevState.sortAsc,
    }))
  } 
  render() {
    const { sortAsc } = this.state
    return (
      <List
        results={this.getGameResultsArray() || []}
        handleOnclick={this.dropdownOnClick}
        sortAsc={sortAsc}
      />
    )
  }
}
ListContainer.propTypes = {
  leagueResults: PropTypes.objectOf(PropTypes.any).isRequired,
  contestants: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.object),
  ]).isRequired,
}
export default ListContainer;