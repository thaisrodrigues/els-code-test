const initialState = [];

export default function(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_CONTESTANTS':
      return {
        data: action.payload || state,
      };
    case 'ERROR':
    return {
      ...state,
      error: action.error || 'Something went wrong' //get the error
    };
    default:
      return state;
  }
}