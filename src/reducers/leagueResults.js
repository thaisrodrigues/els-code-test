const initialState = {
  data: [],
};
export default function(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_RESULTS':
      return {
        ...state,
        data: action.payload
      };
    case 'ERROR':
    return {
      ...state,
      error: action.error || 'Something went wrong' //get the error
    };
    default:
      return state;
  }
}