const initialState = {
  tournamentName: '',
  tournamentDate: '',
};
export default function(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_TOURNAMENT':
      return {
        tournamentName: action.payload.tournamentName || state.tournamentName,
        tournamentDate: action.payload.tournamentDate || state.tournamentDate,
      };
    case 'ERROR':
    return {
      ...state,
      error: action.error || 'Something went wrong' //get the error
    };
    default:
      return state;
  }
}