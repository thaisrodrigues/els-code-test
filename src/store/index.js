import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import tournamentReducer from '../reducers/tournament';
import leagueResultsReducer from '../reducers/leagueResults';
import contestantsReducer from '../reducers/contestants';

// this is necessary to make the Google chrome extension works.
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; 

export default () => {
  const store = createStore(
    combineReducers({
      tournament: tournamentReducer,
      leagueResults: leagueResultsReducer,
      contestants: contestantsReducer,
    }),
    composeEnhancers(applyMiddleware(thunk))
  );
  return store;
};
