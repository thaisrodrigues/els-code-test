//
// General config for the theme of the app
//
const theme = {
  button: {
    color: '#fff',
    padding: '12px 24px',
    margin: '.3em 0 1em 0',
    lightBlue: '#0595AC',
    darkBlue: '#35bcca',
    hover: {
      lightBlue: '#0f7c8e',
      darkBlue: '#3fa6b1',
    }
  },
  colors: {
    green: '#28B662',
    red: '#E43726',
    gray: '#E2E5EA',
  },
  spacing: '1em',
  borderRadius: '2px',
  borderLeftRadius: '2px 2px 0px 0px',
  fontSize: {
    span: '14px',
    p: '16px',
  }
}
export default theme;